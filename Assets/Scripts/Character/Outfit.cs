﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outfit : MonoBehaviour {
    public Renderer[] slots;
    public Material[] materials;
    public GameObject outfitter;
    public List<int> gear = new List<int>();
    public bool update = false;
    // Use this for initialization
    

    public void Init()
    {
        StartCoroutine(GetOutfit());
    }
    
    void Start()
    {
        if(gear.Count != 5)
        {
            gear = new List<int>();
            gear.Add(0);
            gear.Add(0);
            gear.Add(0);
            gear.Add(0);
            gear.Add(0);
        }
    }

    private void OnEnable()
    {
        if(ES2.Exists(GameManager.instance.localPath + "tmp&tag=gear"))
        {
            gear = ES2.LoadList<int>(GameManager.instance.localPath + "tmp&tag=gear");
            UpdateLook(gear);     
        }
        else
        {
            StartCoroutine(GetOutfit());
        }
        GameManager.instance.outfit = this;
    }

    public void UpdateLook(List<int> data)
    {
        var fitter = Instantiate(outfitter).GetComponent<Outfitter>();
        fitter.loadCharacter(this, data);
        gear = data;
        Destroy(fitter.gameObject);
    }

    virtual public void SetSlot(int slot, Texture texture, int id)
    {
        slots[slot].material.mainTexture = texture;
        gear[slot] = id;
    }
    public void SetSlot(int slot, Material texture, int id)
    {
        slots[slot].material = texture;
        gear[slot] = id;
    }

    public IEnumerator GetOutfit()
    {
        ES2Web web = new ES2Web(GameManager.instance.getPath() + "&tag=" + "outfit");

        yield return StartCoroutine(web.Download());

        if (web.isError)
        {
            if (web.errorCode == "05")
            {
                //no outfit found, Create a default and upload it.
                SetDefaults();
            }
        }
        else
        {
            gear = web.LoadList<int>("outfit");
            Debug.Log("Got Outfit " + gear[0] + " " + gear[1] + " " + gear[2] + " " + gear[3]);
            UpdateLook(gear);
        }
    }

    public IEnumerator UploadOutfit(List<int> data)
    {
        DontDestroyOnLoad(this);
        ES2Web web = new ES2Web(GameManager.instance.getPath() + "&tag=" + "outfit");
        Debug.Log("Initiated Outfit Upload");
        yield return StartCoroutine(web.Upload(data));

        if (web.isError)
        {
            Debug.Log("ErrCode:" + web.errorCode + ": " + web.error );
            if (web.errorCode == "05")
            {

            }
        }
        else
        {
            Debug.Log("uploaded outfit succesfully");
        }
        if (GameManager.instance.outfit != this)
        {
            Destroy(this.gameObject);
        }
    }

    public void SaveOutfit(List<int> data)
    {
        StartCoroutine(UploadOutfit(data));
        ES2.Save(data, GameManager.instance.localPath + "tmp&tag=gear");
        UpdateLook(data);
    }

    private void SetDefaults()
    {
        Debug.Log("Called SetUpdates");
        var gearList = new List<int>();

        for (int i = 0; i < 5; i++)
        {
            gearList.Add(0);
        }
        gear = gearList;
        UpdateLook(gearList);
        StartCoroutine(UploadOutfit(gearList));
    }

}
