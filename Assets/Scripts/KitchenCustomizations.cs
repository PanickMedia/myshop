﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenCustomizations : MonoBehaviour {
    public Material[] counterTops;
    public Material[] Cupboards;
    public Material[] Floors;
    public GameObject[] counterTopsRenderer;
    public GameObject[] CupboardsRenderer;
    public GameObject[] FloorsRenderer;

    private void OnEnable()
    {
        counterTopsRenderer = GameObject.FindGameObjectsWithTag("KitchenCounters");
        CupboardsRenderer = GameObject.FindGameObjectsWithTag("KitchenCupboards");
        FloorsRenderer = GameObject.FindGameObjectsWithTag("KitchenFloors");
    }

    public int pushSlot(int slotSelection, int direction = 1)
    {
        if(slotSelection + direction > 2)
        {
            slotSelection = 0;
        }else if(slotSelection + direction < 0)
        {
            slotSelection = 2;
        }
        else
        {
            slotSelection = slotSelection + direction;
        }
        return slotSelection;
    }

    public int pushSelection(int slotSelection, int currentSlot, int direction = 1)
    {
        Material[] selectedLibrary = null;
        GameObject[] selectedMeshes = null;
        switch (slotSelection)
        {
            case 0:
                selectedLibrary = Floors;
                selectedMeshes = FloorsRenderer;
                break;
            case 1:
                selectedLibrary = Cupboards;
                selectedMeshes = CupboardsRenderer;
                break;
            case 2:
                selectedLibrary = counterTops;
                selectedMeshes = counterTopsRenderer;
                break;
            default:
                Debug.Log("Couldn't find the slotselection" + slotSelection);
                break;
        }
        if(currentSlot + direction < 0)
        {
            currentSlot = selectedLibrary.Length - 1;
        }
        else if (currentSlot + direction >= selectedLibrary.Length)
        {
            currentSlot = 0;
        }
        else
        {
            currentSlot = currentSlot + direction;
        }
        ApplySelection(selectedLibrary[currentSlot], selectedMeshes);
        return currentSlot;
    }

    private void ApplySelection(Material mat, GameObject[] meshes)
    {
        foreach(GameObject rend in meshes)
        {
            rend.GetComponent<Renderer>().material = mat;
        }
    }

    public void LoadKitchen(List<int> data)
    {
        ApplySelection(Floors[data[0]], FloorsRenderer);
        ApplySelection(Cupboards[data[1]], CupboardsRenderer);
        ApplySelection(counterTops[data[2]], counterTopsRenderer);
    }
}
