﻿#pragma strict

var username : String;
var password : String;
var user : JoomlaLogin;


function OnGUI () {

	if (!user.logged){
		GUI.BeginGroup (Rect (Screen.width / 2 - 125, Screen.height / 2 - 100, 250, 200));
		GUI.Box (Rect (0,0,250,150), "Enter your username and password");
		username = GUI.TextField (Rect (10, 30, 230, 20), username, 25);
		password = GUI.TextField (Rect (10, 65, 230, 20), password, 25);
		if(GUI.Button (Rect (75,90,100,25), "Login")){
			if(String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password)){
				GUI.Label(Rect(10,120,230,20), "User/password empty");
			}else{
				user.CheckUser(username, password);				
			}
		}
		if(user.status == 0){
			GUI.Label(Rect(10,120,230,20), "User/password don't match");
		}
		if(user.status == 1){
			GUI.Label(Rect(10,120,230,20), "Unactive account");
		}
		GUI.EndGroup ();
	}else{
		var welcometext : String =  "Welcome " + user.fullname;
		GUI.Label(Rect(10,10,Screen.width,20), welcometext);
	}
	
}