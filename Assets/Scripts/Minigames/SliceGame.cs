﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceGame : MiniGame {
    public enum SlicedFoods {
        Tomato
    }
    public bool slice = true;
    public Renderer[] fruitPieces;
    private bool clamped = false;


    public override void StartGame()
    {
        base.StartGame();
        GameManager.instance.SetIcon(GameManager.instance.AButton);
    }

    public override void Update()
    {
        base.Update();
        if(!wait && GameManager.instance.ControlTarget == this.gameObject)
        { 
            if(slice)
            {
                GameManager.instance.SetIcon(GameManager.instance.AButton);
            }
            else
            {
                GameManager.instance.SetIcon(GameManager.instance.RightArrow);
            }
            if (!slice && !clamped && Input.GetAxis("Horizontal") > 0)
            {
            //    animator.SetTrigger("push");
                actions++;
                slice = true;
                wait = true;
                clamped = true;
            }
            if (slice && Input.GetButtonDown("A") && wait == false)
            {
                if(first)
                {
                    startTime = Time.time;
                    first = false;
                    clamped = true;
                }
            //    animator.SetTrigger("push");
                actions++;
                slice = false;
                wait = true;
            }
            if (clamped && Input.GetAxis("Horizontal") == 0)
            {
                clamped = false;
            }
        }

    }
    public override void EndGame()
    {
        base.EndGame();
        slice = true;
    }

    public override void RestartGame(bool triggers = true)
    {
        Debug.Log("called tomato restart");
        base.RestartGame();
        slice = true;
    }


}
