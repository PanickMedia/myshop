﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeasureGame : MiniGame {
    public float fill;
    public bool running;
    public bool overflow = false;
    public MeasureBoard board;
    public Animator source;
    public Animator destination;
    public AnimatorStateInfo stateinfo;
    public ParticleSystem fillSystem;
    public ParticleSystem emptySystem;
    public int state;
    // Update is called once per frame
    public override void Update () {
        if (GameManager.instance.ControlTarget == gameObject)
        {
            if (fill > 120)
                fill = 120;
            if(!overflow && fill >= 112)
            {
                overflow = true;
                InvokeRepeating("Overflow", 0, 0.3f);
            }
            if(overflow && fill < 112)
            {
                overflow = false;
                CancelInvoke("Overflow");
            }
            stateinfo = board.Destination.GetCurrentAnimatorStateInfo(0);
            var scaledTime = (Mathf.Clamp01(stateinfo.normalizedTime) * 120);
            if (Mathf.Round(scaledTime) == Mathf.Round(fill) )
            {
                board.Destination.SetFloat("direction", 0);
            }
            else if (fill < scaledTime)
            {
                board.Destination.SetFloat("direction", -1);
            }
            else if(fill > scaledTime)
            {
                board.Destination.SetFloat("direction", 1);
            }

            var vertical = Input.GetAxis("Vertical");
            base.Update();

            if (!running && fill > 0 && Input.GetAxis("Vertical") < 0)
            {
                running = true;
                InvokeRepeating("AddFluid", 0, 0.25f);
            }
            if (running && Input.GetAxis("Vertical") == 0)
            {
                running = false;
                CancelInvoke("AddFluid");
            }
            if (state == 0)
            {
                if(vertical > 0.8)
                {
                    state = 2;
                }
                else if (vertical > 0.5)
                {
                    state++;
                }

            }
            if (state == 1)
            {
                if (vertical > 0.8)
                {
                    state++;
                }
                else if(vertical <= 0.5)
                {
                    state--;
                }
            }
            if (state == 2)
            {
                if(vertical <= 0.5)
                {
                    state--;
                }
                else if(vertical <= 0.8)
                {
                    state--;
                }

            }
            board.Source.SetInteger("state", state);
            var partCount = 0;
            foreach(ParticleSystem system in board.fillParticle)
            {
                partCount += system.particleCount;
            }
            var totalFill = fill + (partCount * 0.2);
            var modifier = 0.2f;

            if (board.fillTarget > totalFill)
            {
                GameManager.instance.SetIcon("Up");
            }
            else if(board.fillTarget > (totalFill - (totalFill*modifier)) && (board.fillTarget < (totalFill + (totalFill*modifier) ) ) )
            {
                GameManager.instance.SetIcon("A");
            }
            else if(board.fillTarget < totalFill)
            {
                GameManager.instance.SetIcon("Down");
            }
        }

    }

    public override void StartGame()
    {
        base.StartGame();
    }

    private void AddFluid()
    {
        board.emptyParticle.Emit(2);
        fill -= 0.7f;
    }
    private void Overflow()
    {
        board.emptyParticle.Emit(2);
        fill -= 0.3f;
    }

}
