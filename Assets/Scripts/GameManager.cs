﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    private string saveDataPath;
    public string localPath;

    public enum miniGames
    {
        ButtonSequence,
        Spinner,
        Measure,
    }
    public GameObject LoadingOverlay;
    public Text cashIndicator;
    public Sprite AButton;
    public Sprite BButton;
    public Sprite YButton;
    public Sprite XButton;
    public Sprite RightArrow;
    public Sprite UpArrow;
    public Sprite LeftArrow;
    public Sprite DownArrow;
    public Image InstructionImage;
    public Image Panel;
    public static GameManager instance;
    public CompletionScreen CompletedScreen;
    public GameObject ControlTarget;
    public Text timeText;
    public string queuedGame = null;
    public Sprite[] ButtonSequenceSprites;
    public Outfit outfit;
    public CloudStore webSave;
    public Dictionary<string, float> Scores = new Dictionary<string, float>();
    public float inputThreshold = 0.2f;
    public bool upClamp;
    public bool downClamp;
    public bool leftClamp;
    public bool rightClamp;
    public string inputMessage;

    public string[] SceneReferences;

	// Use this for initialization
	void Start () {

        instance = this;
        DontDestroyOnLoad(this);
        //LoadMiniGame("BeatEgg");
        UpdateCash();
        //LoadGame(miniGames.ButtonSequence, "Celery");
        //GetComponent<CloudStore>().init();
    }

    public void setPath(string userID)
    {
        string baseURL = "https://myshop.fun/ES2.php";
        saveDataPath = baseURL + "?webfilename=" + userID + "_session.txt&webusername=xfasfgrwrwwqrwq&webpassword=fds535rqawq2";
    }

    public string getPath()
    {
        return saveDataPath;
    }
	
    public void LoadShop()
    {
        LoadScene("Shop");
    }

	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D) || Input.GetAxis("Mouse X") != 0)
        {
            
        }

        //Direction Code
        var vertical = Input.GetAxis("Vertical");
        var horizontal = Input.GetAxis("Horizontal");
        inputMessage = null;
        if (!upClamp && vertical > inputThreshold)
        {
            upClamp = true;
            inputMessage = "Up";
        }
        if (!rightClamp && horizontal > inputThreshold)
        {
            rightClamp = true;
            inputMessage = "Right";
        }
        if (!leftClamp && horizontal < -inputThreshold)
        {
            leftClamp = true;
            inputMessage = "Left";
        }
        if (!downClamp && vertical < -inputThreshold)
        {
            downClamp = true;
            inputMessage = "Down";
        }
        if (upClamp && vertical <= inputThreshold)
        {
            upClamp = false;
        }
        if (downClamp && vertical >= -inputThreshold)
        {
            downClamp = false;
        }
        if (leftClamp && horizontal >= -inputThreshold)
        {
            leftClamp = false;
        }
        if (rightClamp && horizontal <= inputThreshold)
        {
            rightClamp = false;
        }
        if (inputMessage != null)
        {
            if(ControlTarget != null)
            {
                ControlTarget.SendMessage("InputMessage", inputMessage);
            }
        }
    }

    public void LoadGame(miniGames game, string boardName)
    {
        Debug.Log(boardName);
        switch (game)
        {
            case miniGames.ButtonSequence:
                LoadScene("ButtonSequence");
                break;
            case miniGames.Spinner:
                LoadScene("SpinGame");
                break;
            case miniGames.Measure:
                LoadScene("MeasureGame");
                break;
            default:
                break;
        }
        queuedGame = boardName;
    }

    public virtual IEnumerator LoadScene(int newScene)
    {
        SceneManager.LoadScene(newScene);
        return null;
    }
    public IEnumerator LoadScene(string newScene)
    {
        var i = 0;
        foreach(string scenename in SceneReferences)
        {
            if(scenename == newScene)
            {
                SceneManager.LoadScene(i);
                return null;
            }else
            {
                i++;
            }
        }
        Debug.Log("Found Nothing");
        return null;
    }


    virtual public void SetIcon(Sprite target)
    {
        InstructionImage.gameObject.SetActive(true);
        if (target != null)
            InstructionImage.sprite = target;
        else
            InstructionImage.gameObject.SetActive(false);
    }

    public void SetIcon(string target)
    {
        InstructionImage.gameObject.SetActive(true);
        if (target == "Up")
            InstructionImage.sprite = UpArrow;
        else if (target == "Down")
            InstructionImage.sprite = DownArrow;
        else if (target == "Left")
            InstructionImage.sprite = LeftArrow;
        else if (target == "Right")
            InstructionImage.sprite = RightArrow;
        else if (target == "A")
            InstructionImage.sprite = AButton;
        else if (target == "Cancel")
            InstructionImage.sprite = BButton;
        if (target == "")
            InstructionImage.gameObject.SetActive(false);
    }

    public void UpdateCash()
    {
        cashIndicator.text = PlayerPrefs.GetInt("cash").ToString();
    }
    
    public void SetLoadText(string message)
    {
        Text overlay = LoadingOverlay.transform.Find("Text").GetComponent<Text>();
        overlay.text = message;
    }
}