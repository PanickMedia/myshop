<?php
/* * * * *
 * JoomLogin For Unity3D
 * ------------------------------
 * Developed by Víctor Gil Uría © Brainmedia.es 2013
 * Just upload this file to your joomla root folder
 * * * * */

	define( '_JEXEC', 1 );
	define( 'JPATH_BASE', realpath(dirname(__FILE__)));
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	$mainframe = JFactory::getApplication('site');
	$mainframe->initialise();
	if(isset($_REQUEST['u'])){
		$username = $_REQUEST['u'];
//		echo "USER SET " . $username;
	}else{
		$username = '';
//		echo "USER NOT SET";
	}

	if(isset($_REQUEST['p'])){
		$password = $_REQUEST['p'];
//		echo "PW SET " . $password;
	}else{
		$password = '';
//		echo "PW NOT SET";
	}

	$user =& JUser::getInstance($username);
	//var_dump($user);
	if(!is_null($user->email)){
		$status = 1;
	}else{
		$status = 0;
	}
	$result = JFactory::getApplication()->login(array('username'=>$username,    'password'=>$password));
	if($result){
		echo '"status":"2","email":"' . $user->email . '","name":"' . $user->name . '","id":"' . $user->id . '"';
	}else{
		echo '"status":"' . $status .'","email":"' . $user->email . '","name":"' . $user->name . '"';
	}

?>