﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : ControlScheme {
    public Animator animator;
    public CharacterController controller;
    public float speed = 25;

    void Start()
    {
        animator = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        if(GameManager.instance.ControlTarget == null)
        {
            GameManager.instance.ControlTarget = gameObject;
        }
    }


    override public void Update () {
        if(GameManager.instance.ControlTarget == this.gameObject)
        {
            var horizontal = Input.GetAxis("Horizontal");
            var vertical = Input.GetAxis("Vertical");

            Vector3 directionVector = new Vector3(vertical, 0, horizontal * -1);
            controller.Move( (directionVector).normalized * speed * Time.deltaTime);
            animator.SetFloat("speed", controller.velocity.magnitude);

            if(vertical != 0 || horizontal != 0)
            {
                transform.LookAt(transform.position + directionVector * -1);
            }
        }

        base.Update();
	}

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        if (body != null && !body.isKinematic)
            body.velocity += hit.controller.velocity;
    }
}
