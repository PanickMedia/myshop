﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeasureBoard : GameBoard {
    public Animator Source;
    public Animator Destination;
    public float fillTarget;
    public ParticleSystem emptyParticle;
    public ParticleSystem[] fillParticle;
    // Use this for initialization
    public override void Initialize()
    {
        base.Initialize();
        miniGame = GameObject.FindGameObjectWithTag("minigame").GetComponent<MiniGame>();
        miniGame.enabled = true;
        var measuregame = miniGame.GetComponent<MeasureGame>();
        measuregame.board = this;

        miniGame.StartGame();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
