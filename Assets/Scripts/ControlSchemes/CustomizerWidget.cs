﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CustomizerWidget : ControlScheme {
    public Image iconDisplay;
    public GameObject kitchenCustomizations;

    private KitchenCustomizations kitchenData;
    private List<int> currentSelections = new List<int>();
    private int currentSlot = 0;
    public Sprite[] slotSprites;

    private void OnEnable()
    {
        //Construct a new KitchenCustomizations object to read from it
        kitchenData = Instantiate(kitchenCustomizations).GetComponent<KitchenCustomizations>();
        iconDisplay.sprite = slotSprites[currentSlot];
        currentSelections = new List<int>();

    }

    private void OnDisable()
    {
        Destroy(kitchenData);
    }

    // Update is called once per frame
    override public void Update () {
        if (inputMessage == "Up")
        {
            currentSlot = kitchenData.pushSlot(currentSlot, 1);
            iconDisplay.sprite = slotSprites[currentSlot];
        } else if (inputMessage == "Down")
        {
            currentSlot = kitchenData.pushSlot(currentSlot, -1);
            iconDisplay.sprite = slotSprites[currentSlot];
        } else if (inputMessage == "Left")
        {
            currentSelections[currentSlot] = kitchenData.pushSelection(currentSlot, currentSelections[currentSlot], -1);
        } else if (inputMessage == "Right")
        {

            currentSelections[currentSlot] = kitchenData.pushSelection(currentSlot, currentSelections[currentSlot], 1);
        }
        if (Input.GetButtonDown("A"))
        {
            GameManager.instance.ControlTarget = GameObject.FindGameObjectWithTag("Player");
            GameManager.instance.webSave.uploadKitchen(currentSelections);
            gameObject.SetActive(false);
        }


        base.Update();
	}

    
}
