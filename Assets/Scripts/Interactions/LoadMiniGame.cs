﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMiniGame : Interactable {
    public GameManager.miniGames[] gameType;
    public Sprite[] gameSprite;
    public string[] gameName;
    public bool clamped;
    public int selection = 0;
    public override void Update()
    {
        if(!clamped && GameManager.instance.ControlTarget == this.gameObject)
        {
            var horizontal = Input.GetAxis("Horizontal");
            if (horizontal != 0f)
            {
                pushSelection(horizontal);
            }
        }

        if (Input.GetButtonDown("B") && GameManager.instance.Panel.gameObject.activeSelf)
        {
            GameManager.instance.Panel.gameObject.SetActive(false);
            GameManager.instance.ControlTarget = GameManager.instance.outfit.gameObject;
            GameManager.instance.InstructionImage.gameObject.SetActive(false);
        }
        base.Update();
    }
    public override void DoInteraction()
    {
        base.DoInteraction();
        if (GameManager.instance.ControlTarget != this.gameObject)
        {
            if (gameName.Length != 1)
            {
                GameManager.instance.ControlTarget = this.gameObject;
                GameManager.instance.InstructionImage.gameObject.SetActive(true);
                GameManager.instance.SetIcon(gameSprite[selection]);
                GameManager.instance.Panel.gameObject.SetActive(true);
            }else
            {
                GameManager.instance.LoadGame(gameType[selection], gameName[selection]);
                GameManager.instance.Panel.gameObject.SetActive(false);
            }
        }else
        {
            GameManager.instance.LoadGame(gameType[selection], gameName[selection]);
            GameManager.instance.Panel.gameObject.SetActive(false);
        }
    }

    private void pushSelection(float direction)
    {
        if (Mathf.Clamp(direction, -1, 1) == 1)
        {
            if(selection - 1 < 0)
            {
                selection = gameName.Length - 1;
            }else
            {
                selection -= 1;
            }
        }else
        {
            if(selection + 1 > gameName.Length - 1)
            {
                selection = 0;
            }else
            {
                selection = gameName.Length - 1;
            }
        }
        clamped = true;
        GameManager.instance.SetIcon(gameSprite[selection]);
        Invoke("allowInput", 0.5f);
    }

    private void allowInput()
    {
        clamped = false;
    }
}
