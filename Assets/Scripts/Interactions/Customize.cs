﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Customize : Interactable {
    public GameObject customizerWindow;
    public enum targets
    {
        kitchen
    }
    public targets roomTarget;

    public override void DoInteraction()
    {
        base.DoInteraction();
        switch (roomTarget)
        {
            case targets.kitchen:
                customizerWindow.SetActive(true);
                GameManager.instance.ControlTarget = customizerWindow;
                break;
            default:
                break;
        }
    }
}
