﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outfitter : MonoBehaviour {
    public Material[] faces;
    public Material[] skins;
    public Material[] shirts;
    public Material[] pants;
    public Material[] shoes;
    public GameObject[] handHeld;
    // Use this for initialization

    public void loadCharacter(Outfit character, List<int> parameters)
    {
        character.SetSlot(0, faces[parameters[0]], parameters[0]);
        character.SetSlot(1, skins[parameters[1]], parameters[1]);
        character.SetSlot(2, shirts[parameters[2]], parameters[2]);
        character.SetSlot(3, pants[parameters[3]], parameters[3]);
        character.SetSlot(4, shoes[parameters[4]], parameters[4]);
    }

    public void shiftSlot(Outfit character, int slot, int direction)
    {
        int newPosition = GameManager.instance.outfit.gear[slot] + direction;
        var slotMats = slotToInt(slot);
        if (newPosition >= slotMats.Length)
        {
            newPosition = 0;
        }else if(newPosition < 0)
        {
            newPosition = slotMats.Length - 1;
        }else
        {
            
        }
        Debug.Log(direction + " " + slotMats.Length);

        character.SetSlot(slot, slotMats[newPosition], newPosition);
    }

    private Material[] slotToInt (int slot)
    {
        switch (slot)
        {
            case 0:
                return faces;
            case 1:
                return skins;
            case 2:
                return shirts;
            case 3:
                return pants;
            case 4:
                return shoes;
            default:
                return null;
        }
    }

}
