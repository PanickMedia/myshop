﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinGame : MiniGame {

    public string lastInput = null;
    public string nextInput = null;
    public float speed;
    public List<string> sequence = new List<string>();
    public Animator animator;

    public enum sequenceTypes
    {
        cwSpin,
        ccwSpin,
        mashA,
        leftRight,
        upDown,
    }


    public override void Update()
    {
        base.Update();
        if(GameManager.instance.ControlTarget == this.gameObject)
        {
            if(inputMessage != null)
            {
                if (sequence.Contains(inputMessage))
                {
                    lastInput = inputMessage;
                }

                if(nextInput == inputMessage)
                {
                    addBoost();
                    Debug.Log("boosted");
                }
                if ((sequence.IndexOf(inputMessage) + 1) >= sequence.Count)
                    nextInput = sequence[0];
                else
                    nextInput = sequence[sequence.IndexOf(inputMessage) + 1];
            }
        }
    }

    public void loadSequence (sequenceTypes SeqType)
    {
        sequence = new List<string>();
        switch (SeqType)
        {
            case sequenceTypes.cwSpin:
                sequence.Add("Right");
                sequence.Add("Up");
                sequence.Add("Left");
                sequence.Add("Down");
                break;
            case sequenceTypes.ccwSpin:
                sequence.Add("Right");
                sequence.Add("Down");
                sequence.Add("Left");
                sequence.Add("Up");
                break;
            case sequenceTypes.mashA:
                sequence.Add("A");
                break;
            case sequenceTypes.leftRight:
                sequence.Add("Left");
                sequence.Add("Right");
                break;
            case sequenceTypes.upDown:
                sequence.Add("Up");
                sequence.Add("Down");
                break;
        }

    }

    public void addBoost()
    {
        speed += 0.3f;
        animator.speed = speed;

    }

    public void boostDrain()
    {
        if (speed > 0)
            speed -= 0.1f + (0.1f * speed);
        if (speed < 0)
            speed = 0;
        animator.speed = speed;
    }

    public override void StartGame()
    {
        base.StartGame();
        InvokeRepeating("boostDrain", 0f, 0.25f);
    }
    public override void EndGame()
    {
        base.EndGame();
        CancelInvoke("boostDrain");
        sequence = null;
    }
}
