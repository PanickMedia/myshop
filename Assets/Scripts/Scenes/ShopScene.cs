﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopScene : SceneScript {
    public GameObject kitchenDB;

    private void Start()
    {
       // WaitList.Add(FindObjectOfType<Outfit>().gameObject);
       // WaitList.Add(gameObject);
    }

    private void OnEnable()
    {
        if (ES2.Exists(GameManager.instance.localPath + "?tag=kitchen") )
        {
            var kitchenData = ES2.LoadList<int>(GameManager.instance.localPath + "?tag=kitchen");
            if(kitchenData.Count != 3)
            {
                List<int> defaultKitchen = new List<int>();
                defaultKitchen.Add(0);
                defaultKitchen.Add(0);
                defaultKitchen.Add(0);
                ES2.Save(defaultKitchen, (GameManager.instance.localPath + "?tag=kitchen"));
                UpdateLook(defaultKitchen);
            }
            UpdateLook( kitchenData );
        }
        else
        {
            List<int> defaultKitchen = new List<int>();
            defaultKitchen.Add(0);
            defaultKitchen.Add(0);
            defaultKitchen.Add(0);
            ES2.Save(defaultKitchen, (GameManager.instance.localPath + "?tag=kitchen"));
            UpdateLook(defaultKitchen);
        }
        GameManager.instance.LoadingOverlay.SetActive(false);

    }

    public void loadKitchen()
    {

    }

    public void UpdateLook(List<int> data)
    {
        var db = Instantiate(kitchenDB).GetComponent<KitchenCustomizations>();
        db.LoadKitchen(data);
    }
}
