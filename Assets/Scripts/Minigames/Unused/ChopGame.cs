﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChopGame : MiniGame {
    public enum ChoppedFoods
    {
        Celery
    }

    public Dictionary<ChoppedFoods, Sprite> FoodSprites = new Dictionary<ChoppedFoods, Sprite>();
    // Update is called once per frame

    public override void Update () {
        base.Update();
        if (GameManager.instance.ControlTarget == this.gameObject)
        {
            if (!wait)
            {
                GameManager.instance.SetIcon(GameManager.instance.AButton);
            }

            if (!wait && Input.GetButtonDown("A"))
            {
                if(first)
                {
                    first = false;
                    startTime = Time.time;
                }
                wait = true;
                //animator.SetTrigger("push");
                actions++;
            }
        }


    }
    public override void EndGame()
    {
        base.EndGame();

    }


}
