﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudStore : MonoBehaviour {
    public IEnumerator Upload(List<int> appearance, string tag)
    {
        // Create our ES2Web object.
        ES2Web web = new ES2Web(GameManager.instance.getPath() + "&tag=" + tag);

        // Start uploading our data and wait for it to finish.
        yield return StartCoroutine(web.Upload(appearance));

        if (web.isError)
        {
            // Enter your own code to handle errors here.
            Debug.LogError(web.errorCode + ":" + web.error);
        }else
        {

        }
    }
    public IEnumerator Upload(string data, string tag)
    {
        // Create our ES2Web object.
        ES2Web web = new ES2Web(GameManager.instance.getPath() + "&tag=" + tag);

        // Start uploading our data and wait for it to finish.
        yield return StartCoroutine(web.Upload(data));

        if (web.isError)
        {
            // Enter your own code to handle errors here.
            Debug.LogError(web.errorCode + ":" + web.error);
        }
        else
        {

        }
    }
    public IEnumerator Upload(int data, string tag)
    {
        // Create our ES2Web object.
        ES2Web web = new ES2Web(GameManager.instance.getPath() + "&tag=" + tag);
        // Start uploading our data and wait for it to finish.
        yield return StartCoroutine(web.Upload(data));

        if (web.isError)
        {
            // Enter your own code to handle errors here.
            Debug.LogError(web.errorCode + ":" + web.error);
        }
        else
        {

        }
    }
    public IEnumerator UploadOutfit(Outfit outfit)
    {
        ES2Web web = new ES2Web(GameManager.instance.getPath() + "&tag=" + "outfit");

        yield return StartCoroutine(web.Upload(outfit));

        if (web.isError)
        {

        }
        else
        {
            Debug.Log("Uploaded outfit succesfully");
        }
    }
    public IEnumerator Download(string tag, GameObject caller)
    {
        // Create our ES2Web object.
        ES2Web web = new ES2Web(GameManager.instance.getPath() + "&tag=" + tag);
        // Start downloading our data and wait for it to finish.
        yield return StartCoroutine(web.Download());

        if (web.isError)
        {
            // Enter your own code to handle errors here.
            Debug.Log(web.errorCode + ":" + web.error);
            if (web.errorCode == "05")
            {


            }
        }
        else
        {
            if(tag == "gear" || tag == "kitchen")
            {
                var data = web.LoadList<int>(tag);
                caller.SendMessage("UpdateLook", data);
            }else if(tag == "uid")
            {
                var data = web.Load<int>(tag);
            }else if(tag == "username")
            {
                var data = web.Load<string>(tag);
            }
        }
    }

    public void downloadListInt(string tag, GameObject caller)
    {
        StartCoroutine(Download(tag, caller));
        return;
    }

    public void UploadSaveTag(string tag, List<int> data)
    {
        ES2.Save(data, GameManager.instance.localPath + "?tag=" + tag);
        StartCoroutine(Upload(data, tag));
    }

    public void uploadAppearance(List<int> gear)
    {
        ES2.Save(gear, GameManager.instance.localPath + "?tag=" + "gear");
        StartCoroutine(Upload(gear, "gear"));
    }

    public void uploadKitchen(List<int> choices)
    {
        ES2.Save(choices, GameManager.instance.localPath + "?tag=kitchen");
        StartCoroutine(Upload(choices, "kitchen"));
    }

    public IEnumerator DeleteProfile()
    {
        ES2Web web = new ES2Web(GameManager.instance.getPath());
        // Send request to delete our data and wait for it to finish.
        yield return StartCoroutine(web.Delete());

        if (web.isError)
        {
            // Enter your own code to handle errors here.
            // For a list of error codes, see www.moodkie.com/easysave/ES2Web.ErrorCodes.php
            Debug.LogError(web.errorCode + ":" + web.error);
        }else
        {
            GameManager.instance.LoadScene("mainScene");
        }
    }

}