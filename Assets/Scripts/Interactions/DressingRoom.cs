﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DressingRoom : Interactable {

    public override void DoInteraction()
    {
        base.DoInteraction();
        GameManager.instance.LoadScene("ChangeOutfit");
    }
}
