﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGame : ControlScheme {
    public GameBoard[] boards;
    public GameBoard loadedBoard;
    public enum MiniGames
    {
        Slice,
        Chop
    }
    public Transform boardSlot;
    public string label;
    public bool first = true;
    public bool wait;
    public float startTime;
    public float stopTime;
    public int actions;
    public Dictionary<string, GameBoard> Boards = new Dictionary<string, GameBoard>();
    public float inputThreshold = 0.2f;
    public bool upClamp;
    public bool downClamp;
    public bool leftClamp;
    public bool rightClamp;

    // Use this for initialization


    void OnEnable () {
        foreach (GameBoard board in boards)
        {
            Debug.Log(board.name);
            Boards.Add(board.name, board);
        }

        LoadBoard(GameManager.instance.queuedGame);
        GameManager.instance.queuedGame = null;
    }




    // Update is called once per frame
    public override void Update () {

        if (GameManager.instance == null || GameManager.instance.ControlTarget == this.gameObject)
        {
            if (Input.GetButtonDown("Cancel"))
            {
                RestartGame();
            }
        }
        base.Update();
    }
    public void LoadBoard(string board)
    {
        if (loadedBoard != null)
        {
            Debug.Log("Cannot change boards while one is loaded, call UnloadMiniGame() first");
        }
        else
        {
            var targetBoard = Boards[board];
            var newboard = targetBoard.gameObject;
            newboard.SetActive(true);
            loadedBoard = targetBoard;
            targetBoard.Initialize();
            targetBoard.miniGame.RestartGame(false);
            targetBoard.miniGame.label = board;
            if (loadedBoard == null)
            {
                Debug.Log("problem");
            }
        }
    }
    public void UnloadBoard()
    {
        if (loadedBoard != null)
        {
            loadedBoard.gameObject.SetActive(false);
            loadedBoard = null;
        }
    }

    virtual public void StartGame()
    {
        GameManager.instance.ControlTarget = this.gameObject;
    }

    virtual public void EndGame()
    {
        GameManager.instance.ControlTarget = null;
        GameManager.instance.SetIcon("");
        wait = true;
        stopTime = Time.time;
        GameManager.instance.timeText.gameObject.SetActive(true);
        GameManager.instance.timeText.text = (stopTime - startTime).ToString();
        //GameManager.instance.RegisterScore(stopTime - startTime);
    }

    virtual public void CompleteGame()
    {
        var cashgain = loadedBoard.units;
        //! PlayerPrefs.SetInt("cash", PlayerPrefs.GetInt("cash") + cashgain);
        GameManager.instance.UpdateCash();
        GameManager.instance.CompletedScreen.Activate(loadedBoard.icon, loadedBoard.units.ToString(), cashgain.ToString());
    }

    virtual public void RestartGame(bool triggers = true)
    {
        first = true;
        actions = 0;
        GameManager.instance.timeText.gameObject.SetActive(false);
        GameManager.instance.timeText.text = "";
        wait = true;
        StartGame();
    }

    public int calculateCash()
    {
        return actions;
    }
}
