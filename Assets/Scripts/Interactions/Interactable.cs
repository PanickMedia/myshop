﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : ControlScheme {
    public Sprite icon;
    public bool ready;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Controls>())
        {
            GameManager.instance.SetIcon("A");
            ready = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GameManager.instance.SetIcon("");
        ready = false;
    }

    public virtual void DoInteraction()
    {
        GameManager.instance.SetIcon("");
    }

    override public void Update()
    {
        if (ready)
        {
            if (Input.GetButtonDown("A"))
            {
                DoInteraction();
            }
        }
        base.Update();
    }
}
