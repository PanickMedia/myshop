﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillFluid : MonoBehaviour {
    public MeasureBoard board;

    private void OnParticleCollision(GameObject other)
    {
        var measuregame = board.miniGame.GetComponent<MeasureGame>();
        measuregame.fill += 0.2f;
    }
}
