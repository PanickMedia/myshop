﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSequence : MiniGame {
    public string[] sequence;
    public int stage;
    public Animator animator;

    public override void StartGame()
    {
        base.StartGame();
        stage = 0;
        GameManager.instance.SetIcon(sequence[stage]);
        animator.ResetTrigger("reset");
    }

    public override void RestartGame(bool triggers = true)
    {
        base.RestartGame(triggers);
        stage = 0;
        animator.SetTrigger("reset");
    }

    // Update is called once per frame
    public override void Update() {
        if (GameManager.instance != null && wait && GameManager.instance.InstructionImage.gameObject.activeSelf)
        {
            GameManager.instance.SetIcon("");
        }
        if (GameManager.instance == null || GameManager.instance.ControlTarget == this.gameObject)
        {
            if (!wait)
            {
                GameManager.instance.SetIcon(sequence[stage]);
                bool push = false;


                    if (Input.GetButtonDown("A") && (sequence[stage]) == "A")
                    {
                        push = true;
                    }

                {
                    if (!upClamp && sequence[stage] == "Up" && inputMessage == "Up")
                    {
                        push = true;
                    }
                    else if (!downClamp && sequence[stage] == "Down" && inputMessage == "Down")
                    {
                        push = true;
                    }
                    else if (!leftClamp && sequence[stage] == "Left" && inputMessage == "Left")
                    {
                        push = true;
                    }
                    else if (!rightClamp && sequence[stage] == "Right" && inputMessage == "Right")
                    {
                        push = true;
                    }

                }
                if (push == true)
                {
                    animator.SetTrigger("push");
                    stage++;
                    actions++;
                    wait = true;
                    if (first)
                    {
                        startTime = Time.time;
                        first = false;
                    }
                }

                if (stage == sequence.Length)
                {
                    stage = 0;
                }

            }
        }
        base.Update();
    }
}
