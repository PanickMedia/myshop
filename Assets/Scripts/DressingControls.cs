﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DressingControls : ControlScheme {
    public Outfitter fitter;
    public Outfit character;
    public int currentSlot = 0;
    public Transform[] slotPositions;

    public float inputThreshold = 0.2f;
    public bool upClamp;
    public bool downClamp;
    public bool leftClamp;
    public bool rightClamp;

    public Transform cameras;
    // Use this for initialization
    void Start () {
        GameManager.instance.ControlTarget = this.gameObject;
        cameras.position = slotPositions[0].position;
        fitter = GetComponent<Outfitter>();
    }
	
	// Update is called once per frame
	override public void Update () {
        if (GameManager.instance.ControlTarget == this.gameObject)
        {
            if(inputMessage == "Down")
            {
                var newSlotPosition = currentSlot + 1;
                if (newSlotPosition >= slotPositions.Length)
                {
                    currentSlot = 0;
                }else
                {
                    currentSlot = newSlotPosition;
                }
                cameras.position = slotPositions[currentSlot].position;
            }
            if(inputMessage == "Up")
            {
                var newSlotPosition = currentSlot - 1;
                if(newSlotPosition < 0)
                {
                    currentSlot = slotPositions.Length - 1;
                }else
                {
                    currentSlot = newSlotPosition;
                }
                cameras.position = slotPositions[currentSlot].position;
            }
            if(inputMessage == "Right")
            {
                fitter.shiftSlot(character, currentSlot, 1);
            }
            if(inputMessage == "Left")
            {
                fitter.shiftSlot(character, currentSlot, -1);
            }
            if (Input.GetButtonDown("A"))
            {
                character.SaveOutfit(character.gear);
                GameManager.instance.LoadScene("Shop");
            }
        }
        base.Update();
    }
}
