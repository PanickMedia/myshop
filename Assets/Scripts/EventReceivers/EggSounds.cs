﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggSounds : MonoBehaviour {
    public ParticleSystem[] Parts;
    public void PlaySound(Object sound)
    {
        var audioSource = GetComponent<AudioSource>();
        audioSource.clip = (AudioClip)sound;
        audioSource.Play();
    }

    public void StopParticles()
    {
        foreach (ParticleSystem part in Parts)
        {
            if (part.isPlaying)
            {
                part.Stop();
            }
        }
    }

    public void PlayParticles(int index)
    {
        StopParticles();
        Parts[index].Play();

    }

    public void SetWait()
    {
        GetComponent<GameBoard>().miniGame.wait = false;
    }
}
