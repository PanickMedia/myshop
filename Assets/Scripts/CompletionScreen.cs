﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompletionScreen : MonoBehaviour {
    public Image ItemImage;
    public Image StickerImage;
    public Text ItemVal;
    public Text CashVal;
	
    public void Activate(Sprite itemSprite, string itemNum, string cash, Sprite stickerSprite = null)
    {
        GameManager.instance.ControlTarget = this.gameObject;
        ItemImage.sprite = itemSprite;
        StickerImage.sprite = stickerSprite;
        CashVal.text = cash;
        ItemVal.text = itemNum;
        gameObject.SetActive(true);
        GetComponent<AudioSource>().Play();
    }

    private void Update()
    {
        if(GameManager.instance.ControlTarget == this.gameObject)
        {
            if(Input.GetButtonDown("Cancel"))
            {
                gameObject.SetActive(false);
                //GameManager.instance.loadedBoard.miniGame.RestartGame();
            }

            if(Input.GetButtonDown("Submit"))
            {
                GameManager.instance.ControlTarget = null;
                GameManager.instance.LoadScene("Shop");
                gameObject.SetActive(false);

            }
        }
    }

    public void NextGame()
    {


    }
}
