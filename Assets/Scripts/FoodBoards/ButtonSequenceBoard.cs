﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSequenceBoard : GameBoard {
    public string[] sequence;
	// Use this for initialization
	public override void Initialize () {
        base.Initialize();
        miniGame = GameObject.FindGameObjectWithTag("minigame").GetComponent<MiniGame>();
        miniGame.GetComponent<ButtonSequence>().sequence = sequence;
        miniGame.GetComponent<ButtonSequence>().animator = GetComponent<Animator>();
        miniGame.enabled = true;
        miniGame.StartGame();
	}

}
