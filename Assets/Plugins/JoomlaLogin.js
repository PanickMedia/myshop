﻿#pragma strict
/* * * * *
 * JoomLogin For Unity3D
 * ------------------------------
 * Developed by Víctor Gil Uría ©Brainmedia.es 2013
 * Attach this script to a GameObject and it will handle the conecction
 *  
 * * * * * */


var joomloginUrl 	: String = ""; //The full path to the joomlogin.php in your server
var username 		: String = ""; //The username to send
var password		: String = ""; //The password to send
var status			: int;	       //The status of the player: 0 = User/pass don't match 1 = User not active 2 = Successfully logged	  	
var fullname		: String;      //The full name of the user, returned from Joomla
var userID			: int;		   //Joomla user ID
var email			: String;      //The email of the user, returned from Joomla!
var logged 			: boolean = false; //Check ingame if the user is logged


function Start () {
    //We set the status 3 as the user has not attempt to login yet
	status = 3; 

}


/**
* Function CheckUser
* Handles the connection to the JoomLogin php script in the server
* var u : string // The username to send to the server
* var p : string // The password to send to the server
**/
function CheckUser(u : String, p : String){
	username = u;
	password = p;
	var req : WWWForm = new WWWForm();
	req.AddField("u",username);
	req.AddField("p",password);
	var www : WWW = new WWW(joomloginUrl,req);
	yield www;
	if(!String.IsNullOrEmpty(www.error)){
		//If there is an error in the connection to the server
		Debug.Log(www.error);
	}else{
		ParseResponse(www.text);
	}

}

/**
* Function ParseResponse
* Parses the text string with the user data and fills the vars for use in unity
* str : string //The response (IF NOT ERROR) from the server
**/
function ParseResponse(str : String){
	var pieces : String[] = str.Split(','[0]);
	for(var piece : String in pieces){
		piece = piece.Replace("\"", "");
		var sub : String[] = piece.Split(':'[0]);
		switch (sub[0]){
			case 'status':
				status = int.Parse(sub[1]);
				//If status 2 the user is logged
				if(status == 2){
					logged = true;
				}				
			break;
			case 'email':
				email = sub[1];
			break;
			case 'name':
				fullname = sub[1];
			break;
			case 'id':
				userID = parseInt(sub[1]);
		}//End Switch
	}
}