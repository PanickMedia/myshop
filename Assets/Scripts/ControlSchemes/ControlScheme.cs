﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScheme : MonoBehaviour {
    public string inputMessage;
	public virtual void InputMessage(string message)
    {
        inputMessage = message;
    }

    virtual public void Update()
    {
        inputMessage = null;
    }
}
