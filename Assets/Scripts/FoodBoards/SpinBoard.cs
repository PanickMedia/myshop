﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinBoard : GameBoard {
    public SpinGame.sequenceTypes playSeq;
	// Use this for initialization
	public override void Initialize () {
        base.Initialize();
        miniGame = GameObject.FindGameObjectWithTag("minigame").GetComponent<MiniGame>();

        var spingame = miniGame.GetComponent<SpinGame>();
        spingame.animator = GetComponent<Animator>();
        spingame.loadSequence(playSeq);
        miniGame.enabled = true;
        miniGame.StartGame();
        
    }
	

}
