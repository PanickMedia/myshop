Thank you for purchasing JoomLogin.

== Description ==
JoomLogin is a connection bridge between Joomla! CMS and Unity. It lets you use the default user system in Joomla! to login users 
in your game. 

To try the demo scene, you can use demo/demo as the username and password.


== Installation ==

1. Upload the joomlogin.php file located in the /SERVER/ folder to your Joomla! root folder.
2. Add JoomlaLogin component to the GameObject that will handle your user data
3. Set the Joomlogin Url in the inspector to the url where you uploaded the file in step 1 (I.E http://examlple.com/joomlogin.php)
4. If you want the GUI provided in the example scene, dd GuiLogin component to any GameObject in the scene
5. Reference the GO containing JoomlaLogin in the user field in the inspector

If you want to use your custom GUI solution, you can just reference JoomlaLogin in your script
and call JoomlaLogin.CheckUser(username,password); passing the username and password as a string.
Check the GuiLogin.js script and comments to get an idea.

If you have any questions or suggestions you can reach us in
hola@brainmedia.es