﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Login : MonoBehaviour {
    public JoomlaLogin loginCreds;
    public GameObject loginPrompt;

    private bool active = false;
    private string userID = null;
    private void Start()
    {
        init();
    }
    private void Awake()
    {

    }
    public void init()
    {
        GameManager.instance.SetLoadText("Please log in...");
        loginCreds = GetComponent<JoomlaLogin>();
        if ( LoadLastLogged() )
        {
            active = true;
            loginPrompt.SetActive(false);
            //GameManager.instance.LoadShop();
            //  Debug.Log("Loaded Successfully");
            GameManager.instance.LoadingOverlay.SetActive(true);
            GameManager.instance.SetLoadText("Found Account, please wait...");
        }
        else
        {
            Debug.Log("Couldn't find account, please log in");
            loginPrompt.SetActive(true);
        }
    }

    private void Update()
    {
        if (!active && loginCreds.userID != 0)
        {
            active = true;
            userID = loginCreds.userID.ToString();
            ES2.Save<string>(userID.ToString(), "saveData.txt?tag=uid");
            StartCoroutine(UploadSession(userID, GenerateSession() ) );
            GameManager.instance.LoadingOverlay.SetActive(true);
            GameManager.instance.SetLoadText("Please Wait...");
        }
        if (active)
        {
            if (Input.GetButtonDown("Delete Profile"))
            {
                bootLogin();
                StartCoroutine(DeleteProfile());
            }
        }
    }

    public IEnumerator UploadSession(string userID, string session)
    {
            // Create a URL and add parameters to the end of it.
            string myURL = "https://myshop.fun/ES2.php";
            myURL += "?webfilename=" + userID + "_session.txt&webusername=xfasfgrwrwwqrwq&webpassword=fds535rqawq2";
            var tag = "session";
            // Create our ES2Web object.
            ES2Web web = new ES2Web(myURL + "&tag=" + tag);
            // Start uploading our data and wait for it to finish.
            yield return StartCoroutine(web.Upload(session));

            if (web.isError)
            {
                // Enter your own code to handle errors here.
                Debug.LogError(web.errorCode + ":" + web.error);
            }
            else
            {
                ES2.Save<string>(session, "saveData.txt?tag=session");
                Debug.Log("Saved session: " + myURL + "&tag=" + tag);                
            }
            tag = "lastLogged";
            // Create our ES2Web object.
            web = new ES2Web(myURL + "&tag=" + tag);
            var date = GetDate();
            yield return StartCoroutine( web.Upload(date) );

            if (web.isError)
            {
                // Enter your own code to handle errors here.
                Debug.LogError(web.errorCode + ":" + web.error);
            }
            else
            {
                if(GetComponent<CloudStore>() == null)
                {
                    gameObject.AddComponent<CloudStore>();
                }
                GameManager.instance.setPath(userID);
                GameManager.instance.LoadShop();
                GameManager.instance.localPath = userID + "data";
                ES2.Save<DateTime>(date, GameManager.instance.localPath);
                
                //Debug.Log("Saved date" + myURL + "&tag=" + tag);
            }
     }

    public IEnumerator ValidateSession(string userID, string session)
    {
        // Create a URL and add parameters to the end of it.
        string myURL = "https://myshop.fun/ES2.php";
        myURL += "?webfilename=" + userID + "_session.txt&webusername=xfasfgrwrwwqrwq&webpassword=fds535rqawq2";
        var tag = "session";
        // Create our ES2Web object.
        ES2Web web = new ES2Web(myURL + "&tag=" + tag);

        // Start downloading our data and wait for it to finish.
        yield return StartCoroutine(web.Download());

        if (web.isError)
        {
            // Enter your own code to handle errors here.
            //Debug.Log(web.errorCode + ":" + web.error);
            loginPrompt.SetActive(true);
            if (web.errorCode == "05")
            {
                bootLogin();
            }
        }
        else
        {
            var data = web.Load<string>(tag);
            if(data != session)
            {
                Debug.Log("Data discrepency, please re-log");
                bootLogin();
            }else
            {
                tag = "lastLogged";
                web = new ES2Web(myURL + "&tag=" + tag);

                // Start downloading our data and wait for it to finish.
                yield return StartCoroutine(web.Download());

                if (web.isError)
                {
                    // Enter your own code to handle errors here.
                    Debug.Log(web.errorCode + ":" + web.error);
                    if (web.errorCode == "05")
                    {
                        
                    }
                }
                else
                {
                    var date = web.Load<DateTime>(tag);
                    if (date <= GetDate().AddDays(4))
                    {
                        Debug.Log("This session is valid...");
                        StartCoroutine(UploadSession(userID, GenerateSession()));
                    }
                }
            }
        }
    }

    private bool LoadLastLogged()
    {
        if(ES2.Exists("saveData.txt?tag=uid"))
        {
            userID = ES2.Load<string>("saveData.txt?tag=uid");
            StartCoroutine(ValidateSession(userID, ES2.Load<string>("saveData.txt?tag=session")));
            return true;
        }else
        {
            return false;
        }
        
    }

    private string GenerateSession()
    {
        System.Random rnd = new System.Random();
        byte[] b = new byte[16];
        rnd.NextBytes(b);
        return Convert.ToBase64String(b);
    }

    private DateTime GetDate()
    {
        DateTime currentDate = DateTime.Now;
        return currentDate;
    }

    public IEnumerator DeleteProfile()
    {
        ES2Web web = new ES2Web(GameManager.instance.getPath());
        //Send request to delete our data and wait for it to finish.
        yield return StartCoroutine(web.Delete());

        if (web.isError)
        {
            // Enter your own code to handle errors here.
            // For a list of error codes, see www.moodkie.com/easysave/ES2Web.ErrorCodes.php
            Debug.LogError(web.errorCode + ":" + web.error);
        }
        else
        {

        }

    }

    private void bootLogin()
    {
        active = false;
        ES2.DeleteDefaultFolder();
        ES2.Delete(GameManager.instance.localPath);
        StopAllCoroutines();
        loginPrompt.SetActive(true);
    }
}
